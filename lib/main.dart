import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Column ui - test code',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ScrollController? controller;
  TextEditingController inputCol1 = TextEditingController(text: '1');
  TextEditingController inputCol3 = TextEditingController(text: 'true');
  String inputCol2 = "0";

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("isPrime"), value: "0"),
      DropdownMenuItem(child: Text("isFibonacci"), value: "1"),
    ];
    return menuItems;
  }

  bool isPrime(int n) {
    for (var i = 2; i <= n / i; ++i) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }

  bool isFibonacci(int n) {
    int a = 0;
    int b = 1;
    if (n == a || n == b) return true;
    int c = a + b;
    while (c <= n) {
      if (c == n) return true;
      a = b;
      b = c;
      c = a + b;
    }
    return false;
  }

  onCheckNumber() {
    bool res = false;
    if (inputCol2 == '0') {
      res = isPrime(int.parse(inputCol1.text));
    } else if (inputCol2 == '1') {
      res = isFibonacci(int.parse(inputCol1.text));
    }

    setState(() {
      inputCol3.text = '$res';
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SizedBox(
        child: SingleChildScrollView(
          controller: controller,
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                width: 200.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextField(
                      controller: inputCol1,
                      onSubmitted: (e) {
                        if (double.tryParse(e) != null) {
                          inputCol1.text = (double.parse(e)).round().toString();
                          if (double.parse(e).isNegative) inputCol1.text = '1';
                        }
                        if (int.parse(e).isNegative) inputCol1.text = '1';
                        onCheckNumber();
                      },
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'number',
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                width: width - 200 - 300 < 100 ? 100 : width - 200 - 300,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DropdownButton(
                      value: inputCol2,
                      items: dropdownItems,
                      onChanged: (Object? value) {
                        setState(() {
                          inputCol2 = value.toString();
                        });
                        onCheckNumber();
                      },
                    )
                  ],
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                width: 300.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text(inputCol3.text)],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
